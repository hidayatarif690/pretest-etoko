<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>List Product</title>
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css" rel="stylesheet">
</head>
<body class="antialiased">
    <div class="relative sm:flex sm:justify-center sm:items-center min-h-screen bg-dots-darker bg-center bg-gray-100 dark:bg-dots-lighter dark:bg-gray-900 selection:bg-red-500 selection:text-white">

        <div class="max-w-7xl mx-auto p-6 lg:p-8">
            <div class="mb-4">
                <input type="text" id="searchInput" placeholder=" Cari produk" class="px-4 py-2 shadow-md rounded overflow-hidden">
                <button onclick="searchProducts()" class="bg-green-500 text-white px-4 py-2 shadow-md rounded overflow-hidden">Cari</button>
            </div>

            <div>
                <label for="minPrice">Harga Minimum : </label>
                <input type="number" id="minPrice" placeholder=" harga minimum" class="px-4 py-2 shadow-md rounded overflow-hidden">
                &nbsp;
                <label for="maxPrice">Harga Maksimum : </label>
                <input type="number" id="maxPrice" placeholder=" harga maximum" class="px-4 py-2 shadow-md rounded overflow-hidden">
                <button onclick="filterProducts()" class="bg-green-500 text-white px-4 py-2 shadow-md rounded overflow-hidden">Filter</button>
            </div>
            <br>

            <div id="productsList" class="flex justify-center flex-wrap">
                @foreach($product as $item)
                    <div class="max-w-sm mx-auto bg-white shadow-md rounded overflow-hidden mb-4 mr-2 ml-2">
                        <div class="p-4">
                            <h5 class="text-xl font-semibold mb-2 text-center">{{ $item->nama }}</h5>
                            <img src="{{ asset('storage/' . $item->image) }}" alt="{{ $item->nama }}" class="w-full h-48 object-cover object-center rounded-t text-center">
                            <p class="text-gray-700 text-center">Description : {{ $item->desc}}</p>
                            <h6 class="text-md font-semibold mb-2 text-center"> Price : Rp. {{ $item->harga }}</h6>
                            <p class="text-gray-700 text-center">Stock : {{ $item->stok}}</p>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="flex justify-center mt-16 px-0 sm:items-center sm:justify-between">
                <div class="text-center text-sm sm:text-left">
                    &nbsp;
                </div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script>
            function searchProducts() {
                let name = document.getElementById('searchInput').value;
                
                $.ajax({
                    type: 'GET',
                    url: '/products/search',
                    data: { name: name },
                    success: function(response) {
                        displayProducts(response);
                    },
                    error: function(error) {
                        console.error(error);
                    }
                });
            }
        
            function filterProducts() {
                let minPrice = document.getElementById('minPrice').value;
                let maxPrice = document.getElementById('maxPrice').value;
        
                $.ajax({
                    type: 'GET',
                    url: '/products/search',
                    data: { min_price: minPrice, max_price: maxPrice },
                    success: function(response) {
                        displayProducts(response);
                    },
                    error: function(error) {
                        console.error(error);
                    }
                });
            }
        
            function displayProducts(products) {
                let productsList = document.getElementById('productsList');
                productsList.innerHTML = '';
        
                products.forEach(function(product) {
                    let productDiv = document.createElement('div');
                    productDiv.className = "max-w-sm mx-auto bg-white shadow-md rounded overflow-hidden mb-4 mr-2 ml-2";
                    productDiv.innerHTML = `
                        <div class="p-4">
                            <h5 class="text-xl font-semibold mb-2 text-center">${product.nama}</h5>
                            <img src="{{ asset('storage/') }}/${product.image}" alt="${product.nama}" class="w-full h-48 object-cover object-center rounded-t text-center">
                            <p class="text-gray-700 text-center">Description : ${product.desc}</p>
                            <h6 class="text-md font-semibold mb-2 text-center"> Price : Rp. ${product.harga}</h6>
                            <p class="text-gray-700 text-center">Stock : ${product.stok}</p>
                        </div>
                        `;
                    productsList.appendChild(productDiv);
                });
            }
        </script>
    </div>
</body>
</html>
