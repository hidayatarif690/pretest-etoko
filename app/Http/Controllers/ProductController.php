<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $product = Product::all();

        return response()->json([
            "success" => true,
            "message" => "List Product",
            "data" => $product
        ]);
    }

    public function getall()
    {
        $product = Product::all();

        return View::make('product.index')->with('product', $product);
    }

    public function search(Request $request)
    {
        try{
            // dd($request->all());
            $query = Product::query();

        if ($request->has('name')) {
            $query->where('nama', 'like', '%' . $request->input('name') . '%');
        }

        if ($request->has('min_price')) {
            $query->where('harga', '>=', $request->input('min_price'));
        }

        if ($request->has('max_price')) {
            $query->where('harga', '<=', $request->input('max_price'));
        }

        $products = $query->get();

        return response()->json($products);
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'nama' => 'required',
            'harga' => 'required',
            'desc' => 'required',
            'image' => 'required',
            'stok' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error', $validator->errors());
        }

        $product = Product::create($input);

        return response()->json([
            "success" => true,
            "message" => "Product created successfully",
            "data" => $product
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    // public function store(Request $request)
    // {
    //     //
    // }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $product = Product::find($id);

        if(is_null($product)){
            return $this->sendError('Product not found');
        }

        return response()->json([
            "success" => true,
            "message" => "Product retrived successfully",
            "data" => $product
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    // public function edit(Product $product)
    // {
    //     //
    // }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Product $product)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'nama' => 'required',
            'harga' => 'required',
            'desc' => 'required',
            'image' => 'required',
            'stok' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error', $validator->errors());
        }

        $product->nama = $input['nama'];
        $product->harga = $input['harga'];
        $product->desc = $input['desc'];
        $product->image = $input['image'];
        $product->stok = $input['stok'];
        $product->save();

        return response()->json([
            "success" => true,
            "message" => "Product updated successfully",
            "data" => $product
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return response()->json([
            "success" => true,
            "message" => "Product deleted successfully",
            "data" => $product
        ]);
    }
}
