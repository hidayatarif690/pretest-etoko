<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // Product::factory(10)->create();

        Product::insert([
            [
                'nama' => 'Vaseline',
                'harga' => 12000,
                'desc' => 'Body Lotion',
                'image' => 'vaseline.jpg',
                'stok' => 100
            ],
            [
                'nama' => 'Kaft',
                'harga' => 37500,
                'desc' => 'Face Wash',
                'image' => 'kaft.jpg',
                'stok' => 100
            ],
            [
                'nama' => 'Hansaplas',
                'harga' => 5500,
                'desc' => 'Plester luka',
                'image' => 'hansaplas.jpg',
                'stok' => 100
            ],
            [
                'nama' => 'Posh Men',
                'harga' => 47500,
                'desc' => 'Parfum Pria',
                'image' => 'posh.jpg',
                'stok' => 100
            ],
        ]);
    }
}
